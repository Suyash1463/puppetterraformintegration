# Puppet Configuration with Terraform by Suyash Shinde

## Introduction

This repository contains the code and configuration files for completing an assignment on configuring Puppet with Terraform. The main objective of this assignment is to demonstrate the integration of infrastructure provisioning using Terraform with configuration management using Puppet. By automating the setup of Puppet Master and Puppet Client instances, we ensure consistency, scalability, and reliability in managing infrastructure configurations.

## Steps Taken

### 1. Initial Setup

- **Environment Configuration**: Configured AWS credentials and Terraform provider settings for the desired AWS region.
- **Terraform Code**: Developed Terraform code to provision EC2 instances for Puppet Master and Puppet Client.

### 2. Terraform Configuration

- **Security Groups**: Defined AWS security groups for Puppet Master and Puppet Client instances to control inbound and outbound traffic.
- **Instance Provisioning**: Specified instance type, key pair, and security group for Puppet Master and Puppet Client instances.
- **Provisioners**: Utilized remote-exec provisioners to execute commands on the provisioned instances for Puppet installation and configuration.

### 3. Puppet Configuration

- **Puppet Manifests**: Created Puppet manifest files (.pp) to define configurations for Puppet Master and Puppet Client instances.
- **Site.pp**: Main manifest file that includes other manifest files and declares resources.
- **Base Configuration**: Defined basic configurations such as installing NTP service.
- **Webserver Configuration**: Configured Apache service for Puppet Master or Puppet Client as a web server.
- **Client Configuration**: Set up additional configurations specific to the Puppet Client instance.

### 4. Manual Configuration (Optional)

- **Security Groups**: Manually created AWS security groups for Puppet Master and Puppet Client if not already existing, and specified their IDs in Terraform code.

## Motive of the Assignment

The main motive of this assignment is to demonstrate the following:

- **Infrastructure as Code (IaC)**: Provisioning infrastructure using Terraform, enabling automation, repeatability, and version control.
- **Configuration Management**: Configuring server infrastructure using Puppet, ensuring consistency and scalability in managing configurations.
- **Integration of Tools**: Integrating Terraform and Puppet to streamline the process of infrastructure provisioning and configuration management.

