provider "aws" {
  region = "ap-south-1" # Specifies the desired AWS region
}

# Define existing security groups for Puppet Master and Puppet Client
resource "aws_security_group" "puppet_master_sg" {
  id = "sg-0e1d5577de6aa33d5" # Specifies the ID of the manually created security group for Puppet Master
}

resource "aws_security_group" "puppet_client_sg" {
  id = "sg-0d3b2bf037472a906" # Specifies the ID of the manually created security group for Puppet Client
}

# Provision Puppet Master instance
resource "aws_instance" "puppet_master" {
  ami             = "ami-02d0a1cbe2c3e5ae4" # Specifies the AMI ID for Puppet Master
  instance_type   = "t2.micro"              # Specifies the instance type for Puppet Master
  key_name        = "Suyash_Key"            # Specifies the key pair name for SSH access
  security_groups = [aws_security_group.puppet_master_sg.id] # Associates the Puppet Master instance with its security group

  provisioner "remote-exec" {
    inline = [
      "echo '${aws_instance.puppet_master.private_ip} puppet p1' | sudo tee -a /etc/hosts",      # Modifies the hosts file to include Puppet Master's IP
      "curl -O https://apt.puppetlabs.com/puppet6-release-bionic.deb",                           # Downloads Puppet Master packages
      "sudo dpkg -i puppet6-release-bionic.deb",                                                 # Installs Puppet Master packages
      "sudo apt-get update",                                                                     # Updates the package list
      "sudo apt-get install puppetserver -y",                                                    # Installs Puppet Server
      "sudo sed -i 's/JAVA_ARGS=.*/JAVA_ARGS=\"-Xms512m -Xms512m\"/' /etc/default/puppetserver", # Adds JAVA_ARGS to Puppet Server configuration
      "sudo ufw allow 8140",                                                                     # Allows traffic on port 8140
      "sudo systemctl enable puppetserver.service",                                              # Enables Puppet Server service
      "sudo systemctl start puppetserver.service",                                               # Starts Puppet Server service
      "sudo systemctl status puppetserver.service",                                              # Verifies the status of Puppet Server service
      "sudo /opt/puppetlabs/bin/puppetserver ca list",                                           # Lists certificate requests
      # Retrieves Puppet Agent certificate name and stores it in a temporary file
      "sudo /opt/puppetlabs/bin/puppet agent --configprint certname > /tmp/agent_certname.txt",
      # Signs certificate requests
      "sudo /opt/puppetlabs/bin/puppetserver ca sign --certname $(cat /tmp/agent_certname.txt)",
      # Applies Puppet manifest
      "sudo /opt/puppetlabs/bin/puppet apply /etc/puppetlabs/code/environments/production/manifests/site.pp",
    ]
  }
}

# Provision Puppet Client instance
resource "aws_instance" "puppet_client" {
  ami             = "ami-02d0a1cbe2c3e5ae4" # Specifies the AMI ID for Puppet Client
  instance_type   = "t2.micro"              # Specifies the instance type for Puppet Client
  key_name        = "Suyash_Key"            # Specifies the key pair name for SSH access
  security_groups = [aws_security_group.puppet_client_sg.id] # Associates the Puppet Client instance with its security group

  provisioner "remote-exec" {
    inline = [
      "echo '${aws_instance.puppet_master.private_ip} puppet' | sudo tee -a /etc/hosts", # Modifies the hosts file to include Puppet Master's IP
      "curl -O https://apt.puppetlabs.com/puppet6-release-bionic.deb",                   # Downloads Puppet Agent packages
      "sudo dpkg -i puppet6-release-bionic.deb",                                         # Installs Puppet Agent packages
      "sudo apt-get update",                                                             # Updates the package list
      "sudo apt-get install puppet-agent -y",                                            # Installs Puppet Agent
      "sudo systemctl enable puppet",                                                    # Enables Puppet Agent service
      "sudo systemctl restart puppet",                                                   # Restarts Puppet Agent service
      "sudo systemctl status puppet",                                                    # Verifies the status of Puppet Agent service
      "sudo /opt/puppetlabs/bin/puppet agent --test",                                    # Triggers Puppet Agent to apply changes
      "sudo cat /tmp/puppet_test.txt",                                                   # Checks the content of the test file
    ]
  }
}

# Outputs the public IP of Puppet Master and Puppet Client instances
output "puppet_master_public_ip" {
  value = aws_instance.puppet_master.public_ip
}

output "puppet_client_public_ip" {
  value = aws_instance.puppet_client.public_ip
}
