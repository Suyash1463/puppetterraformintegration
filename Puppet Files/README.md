# Puppet Manifests

## Introduction

This directory contains Puppet manifest files (.pp) used for configuring Puppet Master and Puppet Client instances in the assignment. Puppet manifests are used to define the desired state of the system and manage configurations across multiple servers.

## Manifest Files

### 1. site.pp

- **Purpose**: Main manifest file that includes other manifest files and declares resources.
- **Content**: Specifies the configurations to be applied to Puppet Master and Puppet Client instances.

### 2. base_config.pp

- **Purpose**: Defines basic configurations to be applied to all instances, such as installing NTP service.
- **Content**: Contains Puppet code to ensure NTP service is installed and configured.

### 3. webserver_config.pp

- **Purpose**: Configures the Puppet Master or Puppet Client as a web server using Apache.
- **Content**: Includes Puppet code to install Apache web server and configure virtual hosts.

### 4. client_config.pp

- **Purpose**: Sets up additional configurations specific to the Puppet Client instance.
- **Content**: Contains Puppet code to define client-specific configurations, such as custom packages or services.

## Usage

To use these Puppet manifests:

1. Ensure Puppet is installed on the Puppet Master and Puppet Client instances.
2. Place the manifest files in the appropriate Puppet module directories.
3. Apply the configurations using the Puppet agent or Puppet Server.

## Motive

These Puppet manifest files are designed to automate the configuration of Puppet infrastructure components, ensuring consistency and reliability in managing server configurations.
