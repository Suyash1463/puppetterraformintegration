# webserver_config.pp

# Ensure Apache service is installed and running
package { 'apache2':
  ensure => installed,
}

service { 'apache2':
  ensure  => running,
  enable  => true,
}
