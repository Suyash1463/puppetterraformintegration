# base_config.pp

# Ensure NTP service is installed and running
package { 'ntp':
  ensure => installed,
}

service { 'ntp':
  ensure  => running,
  enable  => true,
}
